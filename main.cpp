#include <iostream>

#include "median_finder.h"



int main(){
    MedianFinder<float> mf;

    for (int i = 0; i < 200; i++){
        mf.insert(200 -float(i) /10 - .001);
    }
    std::cout<<"RESULT: "<< mf.get_median()<<std::endl;
}