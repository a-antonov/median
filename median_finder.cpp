#include <iostream>
#include <assert.h>
#include <utility>  

#include "median_finder.h"

using namespace std;

template <typename T>
void MedianFinder<T>::insert(const T& elem){
    data.push_back(elem);
}


template <typename T>
void MedianFinder<T>::print(){
    for (T & elem: data)
        cout << elem << "  ";
    cout << endl;
}


template <typename T>
T MedianFinder<T>::get_median(){
    assert(data.size());
    unsigned int median_point = int(data.size() / 2);
    return get_kth_rank(median_point);
}


template <typename T>
T MedianFinder<T>::get_pivot_point(){
    MedianFinder<T> mf;
    for(unsigned int i = 0; i + 4 < data.size(); i += 5)
        mf.insert(naive_kth_rank(2, i, i + 5));
    return mf.get_median();
}


template <typename T>
T MedianFinder<T>::naive_kth_rank(unsigned int rank, unsigned int start_pos, unsigned int end_pos){
    
    for(unsigned int i = start_pos; i < start_pos + rank + 1; ++i){
        // print();
        unsigned int smallest_idx = i;
        for(unsigned j = i + 1; j < end_pos; ++j)
            if (data[j] < data[smallest_idx])
                smallest_idx = j;
        if( i == smallest_idx )
            continue;
        swap(data[i], data[smallest_idx]);
    }
    return data[start_pos + rank];
}


template <typename T>
T MedianFinder<T>::get_kth_rank(unsigned int k){
    if (data.size() < 10)
        return naive_kth_rank(k, 0, data.size());

    assert(k >= 0 && k < data.size());
    
    T pivot = get_pivot_point();
    MedianFinder<T> smaller, equal, bigger;
    
    for (unsigned int i = 0; i < data.size(); ++i)
        if ( std::abs(data[i] - pivot) < .00001 )
            equal.insert(data[i]);
        else 
            if(data[i] < pivot)
                smaller.insert(data[i]);
        else 
            bigger.insert(data[i]);

    if ( k < smaller.data.size())
        return smaller.get_kth_rank(k);

    if( k < smaller.data.size() + equal.data.size())
        return equal.data[0];

    return bigger.get_kth_rank(k - smaller.data.size() - equal.data.size());
}

template class MedianFinder<float>; 
template class MedianFinder<double>; 