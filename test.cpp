#include <iostream>

#include "median_finder.h"


void test1(){
    MedianFinder<float> mf;
    for (int i = 0; i < 200; i++){
        mf.insert(float(i) /10 - .001);
    }

    std::cout<< "Expected: 9.999" << "     Result: " << mf.get_median() << std::endl; 
}


void test2(){
    MedianFinder<float> mf;
    for (int i = 0; i < 200; i++){
        mf.insert(float(-i) /10 - .001);
    }

    std::cout<< "Expected: -9.901" << "     Result: " << mf.get_median() << std::endl; 
}


void test3(){
    MedianFinder<float> mf;
    for (int i = 0; i < 200; i++){
        mf.insert(10 - .001);
    }

    std::cout<< "Expected: 9.999" << "     Result: " << mf.get_median() << std::endl; 
}

void test4(){
    MedianFinder<double> mf;
    int sign = 1;
    for (int i = 0; i < 20; i++){
        mf.insert( sign *float(i) );
        sign *= -1;
    }

    std::cout<< "Expected: 0" << "     Result: " << mf.get_median() << std::endl; 
}


void test5(){
    MedianFinder<double> mf;
    for (int i = 0; i < 20; i++){
        mf.insert( float(i) + .001 );
        mf.insert(0);
    }

    std::cout<< "Expected: .001" << "     Result: " << mf.get_median() << std::endl; 
    mf.insert(0);
    std::cout<< "Expected: 0" << "     Result: " << mf.get_median() << std::endl; 

}


void test6(){
    MedianFinder<double> mf;
    for (int i = 0; i < 20; i++){
        mf.insert( float(i) + .001 );
        mf.insert(100);
    }

    std::cout<< "Expected: 100" << "     Result: " << mf.get_median() << std::endl; 
}


void test7(){
    MedianFinder<double> mf;
    for (int i = 0; i < 3; i++){
        mf.insert( float(i)  );
    }

    std::cout<< "Expected: 1" << "     Result: " << mf.get_median() << std::endl; 
}


void test8(){
    MedianFinder<double> mf;
    for (int i = 0; i < 100000; i++){
        mf.insert( float(i)  );
    }

    std::cout<< "Expected: 50000" << "     Result: " << mf.get_median() << std::endl; 
}

void test9(){
    MedianFinder<double> mf;
    int sign = 1;
    for (int i = 20; i >= 0; i--){
        mf.insert( sign *float(i) );
        sign *= -1;
    }

    std::cout<< "Expected: 0" << "     Result: " << mf.get_median() << std::endl; 
}


int main(){
    test1();
    test2();
    test3();
    test4();
    test5();
    test6();
    test7();
    test8();
    test9();
    return 0;
}