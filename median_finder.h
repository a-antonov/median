#pragma once
#include <vector>


template <typename T>
class MedianFinder{
    
    std::vector<T> data;

    T get_kth_rank(unsigned int k);
    T naive_kth_rank(unsigned int rank, unsigned int start_pos, unsigned int end_pos);

    T get_pivot_point();
    
public:
    void insert(const T& elem);
    T get_median();
    void print();
};

